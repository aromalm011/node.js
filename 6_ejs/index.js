var express = require("express");
var fs = require("fs");
var app = express();

app.get("/", function (req, res) {
    res.send("This is the Landing Page.")
    res.end();
});

app.get("/about", function (req, res) {
    fs.readFile('about.html', function (err, data) {
        res.writeHead(200, "Ok");
        res.write(data);
        res.end();
    });
});

app.get("/contact", function (req, res) {
    fs.readFile('contact.html', function (err, data) {
        res.writeHead(200, "Ok");
        res.write(data);
        res.end();
    });
});

app.listen(3000, function (err) {
    console.log("App is running");
});