function getUsers(req, res) {
    res.send("GET");
}

function createUsers(req, res) {
    res.send("POST");
}

function updateUsers(req, res) {
    res.send("PUT");
}

function deleteUsers(req, res) {
    res.send("DELETE");
}

module.exports = {
    getUsers,
    createUsers,
    updateUsers,
    deleteUsers
}