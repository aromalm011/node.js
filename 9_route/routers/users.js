const express = require("express");
const router = express.Router();

const userController = require("../controllers/users");

router.get("/", userController.getUsers);
router.post("/", userController.createUsers);
router.put("/", userController.updateUsers);
router.delete("/", userController.deleteUsers);

module.exports = router;