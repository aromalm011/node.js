var express = require("express");
var app = express();

const userRoute = require("./routers/users");

app.use("/", express.static("public"));
app.use("/api/users", userRoute);

app.listen(3000, function(err){
    console.log("App is Running");
});