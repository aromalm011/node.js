var express = require("express");
var app = express();

app.use("/", express.static("public"));

app.get("/api/ashique", function (req, res) {
    res.json({place:"tannur", age: 37 });
});

app.get("/api/aromal", function (req, res) {
    res.json({place:"kottooli", age: 22 });
});

app.listen(3000, function (err) {
    console.log("App is Running");
});
