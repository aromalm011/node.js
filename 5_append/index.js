var http = require("http");
var fs = require("fs");

var server = http.createServer(function (req, res) {
    fs.appendFile('test.txt', 'Hello World!', function (err) {
        if (err) {
            res.writeHead(404, "Not Found");
            res.write("Not Found !");
            return res.end();
        }
        res.writeHead(200, "Ok");
        res.write("File Appended");
        res.end();
    });
});

server.listen(3000);